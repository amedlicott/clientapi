package com.digitalml.service;

import static spark.Spark.*;
import spark.*;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;

import static net.logstash.logback.argument.StructuredArguments.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

public class ClientAPIRouting {

    private static final Logger logger = LoggerFactory.getLogger("clientapi:1");

    public static void main(String[] args) {
    
        port(4567);
    
        get("/ping", (req, res) -> {
            return "pong";
        });
        
        get("/halt", (request, response) -> {
			stop();
			response.status(202);
			return "";
		});
		
        // Handle timings
        
        Map<Object, Long> timings = new ConcurrentHashMap<>();
        
        before(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.put(request, System.nanoTime());
        	}
        });
        
        after(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		long start = timings.remove(request);
        		long end =  System.nanoTime();
        		logger.info("log message {} {} {} {} ns", value("apiname", "clientapi"), value("apiversion", "1"), value("apipath", request.pathInfo()), value("response-timing", (end-start)));
        	}
        });
        
        afterAfter(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.remove(request);
        	}
        });

        get("/client/:id", (req, res) -> {
        
            StringBuffer sb = new StringBuffer();
            sb.append("id = " + req.params("id"));


            return "Get Client Details " + sb.toString();
        });
    }

}